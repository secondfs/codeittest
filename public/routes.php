<?php

$router->get('','PagesController@home');
$router->get('login','LoginController@create');
$router->get('registration','RegisterController@create');

$router->post('login','LoginController@store');
$router->get('logout','LoginController@delete');
$router->post('registration','RegisterController@store');

