<?php

require '../vendor/autoload.php';
require '../core/bootstrap.php';

use App\core\App;
use App\core\Router;
use App\core\Request;
use App\core\databaseInit;

error_reporting(E_ERROR);

//initialize db similar to mine of need
App::get('database')->dbInit();

session_start();
Router::load('routes.php')
    ->direct(Request::uri(), Request::method());

