


<!--    Specification :-->

<!--        Functional requirements:-->
<!--            1. Registration form fields:-->
<!--            а. email;-->
<!--            b. login;-->
<!--            c. real name;-->
<!--            d. password;-->
<!--            e. birth date;-->
<!--            f. country (drop down list taken from database);-->
<!--            g. agree with terms and conditions (checkbox, must be checked by user).-->

<!--    Sign in form fields:-->
<!--        a. login or email;-->
<!--        b. password.-->



<!--    - Should work on PHP 7.0+ or Node v10+ / MariaDB 10+ / HTML5;-->
<!--    - Don’t use frameworks, off-the-shelf registration/login libs;-->
<!--    - You can use 3rd-party mysql libs, validation libs;-->
<!--    - Character encoding is UTF-8 everywhere: MySQL, HTML, PHP/JS;-->
<!--    - You can use TypeScript;-->
<!--    - Try to keep your code well structured;-->
<!--    - Create private repository on bitbucket.org and share it with our account - services@codeit.com.ua. Provide us with url of your repository.-->
<!--    - Deploy your project on any free hosting and provide us with url.-->


<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

<!--    <li>- All fields should be validated before saving to DB;</li>-->
    <li>- Form should remain filled (except password field) in case of validation errors;</li>
    <li>- Email and login must be unique;</li>
<!--    <li>- List of countries should be stored in DB;</li>-->
<!--    <li>- Add user registration Unix timestamp to DB (field type - integer).</li>-->
    <li>- (for PHP) Routing have to work not only in the root of domain, it will be tested on our dev server in separate folder, example:http://site.com/test-task100500/{your routing}</li>
<!--    <li>!Please check this before sending ready task.</li>-->
</body>
</html>