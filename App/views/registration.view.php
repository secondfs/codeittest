<?php require "partials/header.php"?>
    <div class="form-style-6">
        <h1>registration</h1>
        <form method="POST" action="/registration">
            <div>
                <label class="label"  for="email">e-mail</label>
                <input type="email" name="email" value ="<?= $_SESSION['email'] ? $_SESSION['email'] : '' ?>" required>
                <p></p>
            </div>
            <div>
                <label class="label"  for="login">login</label>
                <input type="text" name="login" value="<?=  $_SESSION['login'] ? $_SESSION['login'] : '' ?>" required>
            </div>
            <div>
                <label class="label"  for="realName">real name</label>
                <input type="text" name="realName" value="<?=  $_SESSION['realName'] ? $_SESSION['realName'] : '' ?>" required>
            </div>
            <div>
                <label class="label"  for="password">password</label>
                <input type="password" name="password" required>
            </div>
            <div>
                <label class="label"  for="confirmPassword">Confirm password</label>
                <input type="password" name="confirmPassword" required>
            </div>
            <div>
                <label class="label" for="birthdate">birthdate</label>
                <input type="date" name="birthDate" placeholder="YYYY-MM-DD" value="<?=  $_SESSION['birthDate'] ? $_SESSION['birthDate'] : '' ?>" required>
            </div>
            <div>
                <label class="label"  for="country">country</label >
                <select id="country" name="country"  required>
                    <?php foreach($countries as $country) :?>
                        <option value="<?= $country->name; ?>" selected="<?=  $_SESSION['country'] ? $_SESSION['country'] : '' ?>"><?= $country->name; ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
            <div>
                <input type="checkbox" name="terms" required/> I have read and agree to the Terms and Conditions and Privacy Policy
                <input type="submit" name="submit" value="Registration" />
            </div>
            <div>
                <p style="color: red">
                    <?php foreach ($_SESSION['errors'] as $message) { echo $message."<br/>"; }?>
                </p>
            </div>
        </form>
        <a href="/">Home page</a>
    </div>
<?php require "partials/footer.php"?>