<?php require "partials/header.php"?>
    <div>
        <div class="login">
            <?php if ( $_SESSION['user_login']): ?>
                <a href="logout">logout</a>
            <?php else: ?>
                <a href="login">login</a>
            <?php endif;?>
        </div>
        <div class="registration">
            <a href="/registration">Registration</a>
        </div>
        <div>
            <p>
                <?= $_SESSION['user_login'] ?  "login: ".$_SESSION['user_login']." email: ".$_SESSION['user_email'] : ' '?>
            </p>
        </div>
    </div>
    <?php require "partials/footer.php"?>
