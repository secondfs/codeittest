<?php require "partials/header.php"?>
    <div class="form-style-6">
        <form  method="POST" action="/login">
            <h1>Login</h1>
            <div>
                <label for="email">Email or login</label>
                <div>
                    <input type="text" name="login" value="<?= $_SESSION['login'] ? $_SESSION['login'] : '' ?>" required>
                </div>
            </div>

            <div>
                <label for="password">Password</label>

                <div>
                    <input type="password" name="password" required>
                </div>
            </div>


            <div>
                <div>
                    <button type="submit">
                        Отправить
                    </button>
                </div>
            </div>
            <div>
                <p style="color: red">
                    <?= $_SESSION['errors']['login_errors']?>
                </p>
            </div>
        </form>
        <a href="/">Home page</a>
    </div>
<?php require "partials/footer.php"?>