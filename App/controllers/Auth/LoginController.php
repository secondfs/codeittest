<?php
namespace App\controllers;

use App\core\App;

class LoginController
{
    public function create()
    {
        return view('login');
    }

    public function store()
    {
        $_SESSION['login'] = $_POST['login'];
        $params = [
            'login' => $_POST['login'],
            'password' => password_hash($_POST['password'],PASSWORD_BCRYPT),
        ];
        $user = App::get('database')->select('Users','login',$params);
        if(!$user){
            $_SESSION['errors']['login_errors'] = 'user does not exist';
            return redirect('login');
        }

        $hash = $user->password;

        if(!$this->userVerify($_POST['password'],$hash)){
            $_SESSION['errors']['login_errors'] = 'wrong password';
            return redirect('login');
        }

        $login = $user->login;
        $email = $user->email;

        session_regenerate_id();

        $_SESSION['user_login'] = $login;
        $_SESSION['user_email'] = $email;

        return redirect('');
    }

    public function delete()
    {
        session_destroy();

        redirect('');
    }

    public function userVerify($password,$hash) {
        return password_verify($password, $hash)  ? true : false;
    }
}