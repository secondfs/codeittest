<?php
namespace App\controllers;

use App\core\App;
use Rakit\Validation\Validator;


class RegisterController
{
    public function create()
    {
        $countries = App::get('database')->selectAll('Countries','name');
        return view('registration',compact('countries'));
    }

    public function store()
    {
        $_SESSION = $_POST;


        $validation = $this->validateRequest($_POST);
        if ($validation->fails()) {
            // handling errors
            $errors = $validation->errors();
            $messages = $errors->firstOfAll();
            $_SESSION['errors'] = $messages;


            return redirect('registration');
        }
        $uniqueFields = [
            'email' => $_POST['email'],
            'login' => $_POST['login'],
        ];


        $uniqueLogin = App::get('database')->isUniqueLogin('Users',$uniqueFields['login']);
        $uniqueEmail = App::get('database')->isUniqueEmail('Users',$uniqueFields['email']);

        if(!$uniqueLogin || !$uniqueEmail) {
            $_SESSION['errors'][] = 'Your login or email are exist';
            return redirect('registration');
        }


        App::get('database')->insert('Users',[
            'email' => $_POST['email'],
            'login' => $_POST['login'],
            'password' => password_hash($_POST['password'],PASSWORD_BCRYPT),
            'realName' => $_POST['realName'],
            'birthDate' => $_POST['birthDate'],
            'country' => $_POST['country'],
        ]);

        $login = new LoginController();
        $login->store();

        return redirect('');
    }

    public function validateRequest($request)
    {
        $validator = new Validator();
        $validate =  $validator->validate($_POST, [
            'email' => 'required|email',
            'login' => 'required',
            'password' => 'required|min:6',
            'confirmPassword' => 'required|same:password',
            'realName' => 'required',
            'birthDate' => 'required|before:today',
            'country' => 'required',
            'terms' => 'accepted',
        ]);
        return $validate;
    }
}