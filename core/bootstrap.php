<?php
use App\core\App;
use App\core\QueryBuilder;
use App\core\Connection;

App::bind('config', require __DIR__.'../../config.php');

App::bind('database', new QueryBuilder(
    Connection::make(App::get('config')['database'])
));
function view($name,$data = [])
{
    extract($data);
    return require "../App/views/{$name}.view.php";
}

function redirect($path)
{
    header("Location: /{$path}");
}